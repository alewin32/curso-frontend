import { LitElement, html } from "lit";

class PersonaMainDm extends LitElement {

  static get properties() {
    return {
      people: {type: Array}
    }
  }
  constructor() {
    super();

		this.people = [
			{
				name: "Silvester Stallone",
				yearsInCompany: 1,
				photo: {
					src: './img/persona1.png',
					alt: "Silvester Stallone"
				},
				profile: "Actor americano de peliculas de accion como Rambo"
			},
			{
				name: "Antonio Banderas",
				yearsInCompany: 2,
				photo: {
					src: './img/persona1.png',
					alt: "Antonio Banderas"
				},
				profile: "Actor español de los primeros que se fue a Hollywood"
			},
			{
				name: "Anthony Hopkins",
				yearsInCompany: 3,
				photo: {
					src: './img/persona1.png',
					alt: "Anthony Hopkins"
				},
				profile: "Actor americano que hizo El silencio de los corderos"
			},
			{
				name: "Arnold Schwarzenegger",
				yearsInCompany: 4,
				photo: {
					src: './img/persona1.png',
					alt: "Anthony Hopkins"
				},
				profile: "Actor americano de peliculas de accion, como Terminator"
			},
		];
  }

  updated(changedProperties) {
    console.log("En persona-main-dm, updated")
    if (changedProperties.has("people")) {
      console.log("Ha cambiado el valor de la propiedad people en persona-main-dm");

      this.dispatchEvent(
        new CustomEvent(
          "updated-people", {
            detail: {
              people: this.people
            }
          }
        )
      )
    }
  }

}

customElements.define("persona-main-dm", PersonaMainDm);
