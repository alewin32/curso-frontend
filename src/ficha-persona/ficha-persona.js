import { LitElement, html } from "lit";

class FichaPersona extends LitElement {

  static get properties() {
    return {
      name: {type: String},
      yearsInCompany: {type: Number},
      personInfo: {type: String}
    }
  }

  constructor() {
    super();
    this.name = "Carlos";
    this.yearsInCompany = 12;
    //this.personInfo = "algo";
    //this.updateJob();
  }

  updated(changedProperties) {
    console.log(changedProperties);

    changedProperties.forEach(
      (oldValue, propName) => {
        console.log(" Propiedad " + propName + "ha cambiado de valor, anterior era " + oldValue);

      }
    );

    if (changedProperties.has("name")) {
      console.log("Anterior era " + changedProperties.get("name") + ", ahora es " + this.name)
    }

    if (changedProperties.has("yearsInCompany")) {
      console.log("Anterior era " + changedProperties.get("yearsInCompany") + ", ahora es " + this.yearsInCompany)
      this.updateJob();
    }
  }

	render() {
		return html`
			<div>
        <label>Nombre Completo</label>
        <input type="text" id="fname" value="${this.name}" @input="${this.updateName}"></input>
        <br />
        <label>Años en la empresa</label>
        <input type="text" value="${this.yearsInCompany}" @input="${this.updateAnos}"></input>
        <br />
        <input type="text" value="${this.personInfo}" disabled></input>
        <br />
      </div>
		`
	}

  updateName(e) {
    console.log("updateName")
    console.log(e)
    this.name = e.target.value
  }

  updateAnos(e) {
    console.log("updateAnos")
    console.log(e)
    this.yearsInCompany = e.target.value
  }

  updateJob() {
    if (this.yearsInCompany >= 7) {
      this.personInfo = "lead";
    } else if (this.yearsInCompany >= 5) {
      this.personInfo = "senior";
    } else if (this.yearsInCompany >= 3) {
      this.personInfo = "team";
    } else {
      this.personInfo = "algo";
    }
  }

}

customElements.define("ficha-persona", FichaPersona);
